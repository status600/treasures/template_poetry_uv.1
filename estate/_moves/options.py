

#
#	https://finance.yahoo.com/screener/predefined/most_shorted_stocks
#
#

import pathlib
from os.path import dirname, join, normpath
import sys
this_directory = pathlib.Path (__file__).parent.resolve ()	
def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))
	

add_paths_to_system ([
	'/habitat/venues'
])



#/
#
import arcaic_trading.clouds.Tradier.procedures.options.combine as combine_options  
from arcaic_trading.shows.options._physiques.physique_1.assertions import assert_physique_1 
from arcaic_trading._essence import retrieve_essence
from arcaic_trading.clouds.Tradier.v1.markets.quotes import ask_for_quote
from arcaic_trading.shows.options.pricyness import options_pricyness
from arcaic_trading.procedures.spacedate.days_between import calc_days_between
from arcaic_trading.procedures.spacedate.today import calc_today
from arcaic_trading.procedures.premier_fit_line import calculate_premier_fit_line
#
#
import rich
#
#
import json
from pprint import pprint
#
#\






def ask_API_for_options (
	Tradier_Private_Key,
	symbol
):
	options_chains = combine_options.presently ({
		"symbol": symbol,
		"authorization": Tradier_Private_Key
	})	
	assert_physique_1 (options_chains)
	
	return options_chains;
	
def ask_FS_for_options (symbol):
	file_path = str (normpath (join (this_directory, symbol + ".JSON")))
	with open(file_path, 'r') as json_file:
		data = json.load(json_file)
		return data;
	
def save_options_to_FS (options_chains, symbol):
	file_path = str (normpath (join (this_directory, symbol + ".JSON")))
	with open(file_path, 'w') as json_file:
		json.dump(options_chains, json_file, indent=4)

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
app = dash.Dash (__name__)

def ask_for_options_calculations (packet):
	symbol = packet ["symbol"]

	essence = retrieve_essence ()
	Tradier_Private_Key = essence ["clouds"] ["Tradier"] ["private_key"]
	share_price = ask_API_for_options (Tradier_Private_Key,symbol)
	
	quote = ask_for_quote ({
		"symbol": symbol,
		"authorization": Tradier_Private_Key
	})
	share_price_ask = quote ["ask"]
	

	options_physique_1 = ask_FS_for_options (symbol)
	#options_physique_1 = ask_API_for_options (Tradier_Private_Key,symbol)
	#save_options_to_FS (options_physique_1, symbol)

	
	options = options_pricyness ({
		"options physique 1": options_physique_1,
		"share price": share_price_ask,
		"trade": "ask",
		"today": calc_today ()
	})
	
	return {
		"options": options,
		"share_price_ask": share_price_ask
	};

import plotly.graph_objs as go
import plotly.offline as pyo
def make_figure (packet):
	options_physique_1 = packet ["options_physique_1"]
	symbol = packet ["symbol"]
	share_price_ask = packet ["share_price_ask"]
	
	
	
	days = []
	ratios_from_zero = []
	ratios = []
	for options_at_expiration in options_physique_1:
		expiration = options_at_expiration ["expiration"]
		
		try:
			days_until = options_at_expiration ["calculations"] ["days_until"]
		except Exception:
			days_until = "error"
		
		try:
			calls_to_puts_options_ask_pricyness = options_at_expiration [
				"calculations"
			] ["call:puts options ask pricyness ratio from zero"]
		except Exception:
			calls_to_puts_options_ask_pricyness = "error"
		
		try:
			calls_to_puts_options_ask_pricyness_ratio = options_at_expiration [
				"calculations"
			] ["call:puts options ask pricyness ratio"]
		except Exception:
			calls_to_puts_options_ask_pricyness_ratio = "error"
		
		#print (expiration, days_until, calls_to_puts_options_ask_pricyness)
		print (days_until, calls_to_puts_options_ask_pricyness)
		
		days.append (days_until)
		ratios_from_zero.append (calls_to_puts_options_ask_pricyness)
		ratios.append (calls_to_puts_options_ask_pricyness_ratio)
	
	x_data = days
	y_data = ratios_from_zero

	print ("days:", days)

	# Create a trace
	trace = go.Bar (
		x = x_data,
		y = y_data,
		
		text=ratios,  # Display values on the bars
		textposition='auto',
		
		name = f'{ symbol } @ { share_price_ask }',
		marker = dict(color = '#39F')  # Customize marker color
	)
	
	line_trace = go.Scatter(
		x = x_data,
		y = y2,
		mode='lines+markers',
		name='Line Graph'
	)

	# Create data list
	data = [ trace ]

	# Create layout
	layout = go.Layout (
		title = f'{ symbol } @ { share_price_ask }',
		xaxis = dict(title = 'Days'),
		yaxis = dict(title = 'Ratio')
	)

	# Create Figure object
	fig = go.Figure (data = data, layout=layout)

	# Save the plot (optional, for offline use)
	# pyo.plot(fig, filename='line_graph.html')

	# Display the plot in Jupyter Notebook (if using)
	# pyo.iplot(fig)

	# Display the plot in a web browser (if using a web server)
	#pyo.plot(fig, filename='line_graph.html', auto_open=True)
	
	return fig;


def make_figure_v2 (packet):
	spots_with_fit = packet ["spots_with_fit"]
	symbol = packet ["symbol"]
	share_price_ask = packet ["share_price_ask"]
	
	dates = []
	pricyness = []
	fit = []
	for spot in spots_with_fit:
		print ("spot:", spot)
	
		dates.append (spot ["days_until"])
		
		if ("fit" in spot):
			fit.append (spot ["fit"])
		else:
			fit.append (0)
		
		if ("call:puts pricyness" in spot):
			pricyness.append (spot ["call:puts pricyness"])
		else:
			pricyness.append (0)
		
	
	x_data = dates
	y_data = pricyness
	y2_data = fit

	# Create a trace
	bar_trace = go.Bar (
		x = x_data,
		y = y_data,
		
		text = pricyness,  # Display values on the bars
		textposition = 'auto',
		
		name = f'{ symbol } @ { share_price_ask }',
		marker = dict(color = '#39F')  # Customize marker color
	)
	
	line_trace = go.Scatter(
		x = x_data,
		y = y2_data,
		mode='lines+markers',
		name='Line Graph'
	)

	# Create data list
	#data = [ trace ]

	# Create layout
	layout = go.Layout (
		title = f'{ symbol } @ { share_price_ask }',
		xaxis = dict(title = 'Days'),
		yaxis = dict(title = 'Ratio')
	)

	# Create Figure object
	#fig = go.Figure (data = data, layout=layout)
	
	fig = go.Figure ()
	fig.add_trace (bar_trace)
	#fig.add_trace (line_trace)
	
	
	# Save the plot (optional, for offline use)
	# pyo.plot(fig, filename='line_graph.html')

	# Display the plot in Jupyter Notebook (if using)
	# pyo.iplot(fig)

	# Display the plot in a web browser (if using a web server)
	#pyo.plot(fig, filename='line_graph.html', auto_open=True)
	
	return fig;



def check_1 ():
	print ("check_1");
	
	symbol = "OTLY"
	
	options_calculations = ask_for_options_calculations ({
		"symbol": symbol
	})
	
	share_price_ask = options_calculations ["share_price_ask"]
	options_physique_1 = options_calculations ["options"]
	
	spots_with_fit = calculate_premier_fit_line ({
		"options_physique_1": options_physique_1
	})
	

	app.layout = html.Div([
		html.H1 ('Plotly Graph Example'),
		dcc.Graph(
			id = 'plotly-graph',
			figure = make_figure_v2 ({
				"spots_with_fit": spots_with_fit,
				"share_price_ask": share_price_ask,
				"symbol": symbol
			})
		)
	])
	return;

	#symbol = "BYND"
	#share_price = 6.07
	
	

	
	

	# Initialize the Dash app
	

	# Define the layout of the app
	



	
	#print ("options_chains:", options_chains)

	return;

check_1 ()

if __name__ == '__main__':
   app.run_server (debug=True, port=8000, host='0.0.0.0')