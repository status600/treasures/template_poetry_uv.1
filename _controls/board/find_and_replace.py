
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'../stages_pip'
])


import ships.paths.directory.find_and_replace_string_v2 as find_and_replace_string_v2
import pathlib
from os.path import dirname, join, normpath

this_directory = pathlib.Path (__file__).parent.resolve ()
habitat_path = "/habitat"

places = [
	str (normpath (join (habitat_path, "venue.S.HTML"))),
	str (normpath (join (habitat_path, "pyproject.toml"))),
	str (normpath (join (habitat_path, "venues/stages"))),
]

for place in places:
	print ("place:", place)

	find_and_replace_string_v2.start (
		the_path = place,

		find = 'theme_park',
		replace_with = 'arcaic_trading',
		
		replace_contents = "yes",
		replace_paths = "yes"
	)