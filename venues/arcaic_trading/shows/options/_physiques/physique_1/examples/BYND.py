
''''
from arcaic_trading.shows.options._physiques.physique_1.examples.BYND import ask_for_example
"'''

def ask_for_example ():
	return [
		{
			"expiration": "2024-07-26",
			"calls": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.65,
							"ask": 6.3,
							"last": 5.1
						},
						"contract size": 100,
						"open interest": 12
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 2.63,
							"ask": 5.7,
							"last": 4.85
						},
						"contract size": 100,
						"open interest": 18
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.32,
							"ask": 5.3,
							"last": 3.95
						},
						"contract size": 100,
						"open interest": 13
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 1.81,
							"ask": 4.75,
							"last": 2.25
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 1.61,
							"ask": 4.3,
							"last": 2.0
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 1.28,
							"ask": 1.97,
							"last": 1.2
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.89,
							"ask": 1.18,
							"last": 1.15
						},
						"contract size": 100,
						"open interest": 14
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.63,
							"ask": 0.9,
							"last": 0.6
						},
						"contract size": 100,
						"open interest": 228
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.35,
							"ask": 0.39,
							"last": 0.37
						},
						"contract size": 100,
						"open interest": 2043
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.17,
							"ask": 0.18,
							"last": 0.18
						},
						"contract size": 100,
						"open interest": 4148
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.09,
							"ask": 0.1,
							"last": 0.09
						},
						"contract size": 100,
						"open interest": 2561
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.05,
							"ask": 0.07,
							"last": 0.06
						},
						"contract size": 100,
						"open interest": 1329
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.03,
							"ask": 0.04,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 2455
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.02,
							"ask": 0.03,
							"last": 0.03
						},
						"contract size": 100,
						"open interest": 1015
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.02,
							"ask": 0.05,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 2372
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.2,
							"last": 0.1
						},
						"contract size": 100,
						"open interest": 57
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.02,
							"ask": 0.05,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 7639
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 0.01,
							"ask": 0.5,
							"last": 0.09
						},
						"contract size": 100,
						"open interest": 463
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.03,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 236
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.0,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 75
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.06,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.77,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 177
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.03
						},
						"contract size": 100,
						"open interest": 148
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.02
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.01,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 191
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.06,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 167
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 425
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.03,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 570
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.01,
							"ask": 0.03,
							"last": 0.02
						},
						"contract size": 100,
						"open interest": 2154
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.07,
							"ask": 0.09,
							"last": 0.09
						},
						"contract size": 100,
						"open interest": 2926
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.26,
							"ask": 0.29,
							"last": 0.26
						},
						"contract size": 100,
						"open interest": 4175
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.57,
							"ask": 0.62,
							"last": 0.71
						},
						"contract size": 100,
						"open interest": 2589
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.99,
							"ask": 1.03,
							"last": 1.36
						},
						"contract size": 100,
						"open interest": 1168
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 1.39,
							"ask": 1.63,
							"last": 1.57
						},
						"contract size": 100,
						"open interest": 278
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.65,
							"ask": 2.82,
							"last": 2.05
						},
						"contract size": 100,
						"open interest": 122
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.39,
							"ask": 4.35,
							"last": 2.92
						},
						"contract size": 100,
						"open interest": 25
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 2.74,
							"ask": 4.2,
							"last": 3.43
						},
						"contract size": 100,
						"open interest": 68
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 2.24,
							"ask": 5.55,
							"last": 2.58
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 2.86,
							"ask": 4.8,
							"last": 4.25
						},
						"contract size": 100,
						"open interest": 5299
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 3.3,
							"ask": 6.5,
							"last": 3.65
						},
						"contract size": 100,
						"open interest": 8
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 3.9,
							"ask": 7.05,
							"last": 4.14
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 4.5,
							"ask": 7.55,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 4.8,
							"ask": 8.05,
							"last": 5.51
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 5.3,
							"ask": 8.55,
							"last": 7.18
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 5.75,
							"ask": 9.1,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 6.85,
							"ask": 9.6,
							"last": 8.3
						},
						"contract size": 100,
						"open interest": 20
					}
				]
			}
		},
		{
			"expiration": "2024-08-02",
			"calls": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.0,
							"ask": 6.3,
							"last": 5.63
						},
						"contract size": 100,
						"open interest": 24
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 3.45,
							"ask": 5.75,
							"last": 5.59
						},
						"contract size": 100,
						"open interest": 6
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.62,
							"ask": 3.45,
							"last": 4.25
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 2.16,
							"ask": 4.55,
							"last": 2.55
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 1.71,
							"ask": 3.6,
							"last": 3.1
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 1.21,
							"ask": 3.6,
							"last": 2.65
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.03,
							"ask": 1.45,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.76,
							"ask": 0.81,
							"last": 0.77
						},
						"contract size": 100,
						"open interest": 34
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.46,
							"ask": 0.54,
							"last": 0.49
						},
						"contract size": 100,
						"open interest": 828
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.32,
							"ask": 0.34,
							"last": 0.34
						},
						"contract size": 100,
						"open interest": 495
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.2,
							"ask": 0.23,
							"last": 0.21
						},
						"contract size": 100,
						"open interest": 1404
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.15,
							"ask": 0.16,
							"last": 0.14
						},
						"contract size": 100,
						"open interest": 805
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.09,
							"ask": 0.11,
							"last": 0.1
						},
						"contract size": 100,
						"open interest": 1124
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.06,
							"ask": 0.08,
							"last": 0.08
						},
						"contract size": 100,
						"open interest": 47
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.05,
							"ask": 0.06,
							"last": 0.06
						},
						"contract size": 100,
						"open interest": 296
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.05,
							"last": 0.16
						},
						"contract size": 100,
						"open interest": 17
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.02,
							"ask": 0.04,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 736
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.08,
							"last": 0.06
						},
						"contract size": 100,
						"open interest": 33
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.01,
							"ask": 1.49,
							"last": 0.08
						},
						"contract size": 100,
						"open interest": 67
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 0.0,
							"ask": 1.25,
							"last": 0.14
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.13,
							"last": 0.16
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.01,
							"ask": 0.7,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.83,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 0.01,
							"ask": 1.26,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 39
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.45
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.13,
							"last": 0.02
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.11
						},
						"contract size": 100,
						"open interest": 5
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.01,
							"ask": 0.03,
							"last": 0.02
						},
						"contract size": 100,
						"open interest": 541
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.03,
							"ask": 0.04,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 371
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.1,
							"ask": 0.12,
							"last": 0.12
						},
						"contract size": 100,
						"open interest": 1873
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.25,
							"ask": 0.28,
							"last": 0.26
						},
						"contract size": 100,
						"open interest": 1778
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.49,
							"ask": 0.55,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 2197
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.8,
							"ask": 0.87,
							"last": 0.87
						},
						"contract size": 100,
						"open interest": 1330
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 1.21,
							"ask": 1.25,
							"last": 1.24
						},
						"contract size": 100,
						"open interest": 579
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 1.62,
							"ask": 1.69,
							"last": 1.92
						},
						"contract size": 100,
						"open interest": 27
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 2.01,
							"ask": 2.32,
							"last": 2.18
						},
						"contract size": 100,
						"open interest": 763
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 1.8,
							"ask": 2.84,
							"last": 3.02
						},
						"contract size": 100,
						"open interest": 12
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 2.03,
							"ask": 3.25,
							"last": 3.52
						},
						"contract size": 100,
						"open interest": 7
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 2.28,
							"ask": 4.65,
							"last": 2.62
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 3.9,
							"ask": 4.25,
							"last": 4.45
						},
						"contract size": 100,
						"open interest": 254
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 3.65,
							"ask": 5.75,
							"last": 3.74
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 4.85,
							"ask": 5.2,
							"last": 4.8
						},
						"contract size": 100,
						"open interest": 5
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 4.95,
							"ask": 6.2,
							"last": 5.25
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 5.4,
							"ask": 6.3,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 4.8,
							"ask": 6.95,
							"last": 6.38
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 6.3,
							"ask": 8.9,
							"last": 6.5
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 7.15,
							"ask": 10.1,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			}
		},
		{
			"expiration": "2024-08-09",
			"calls": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.95,
							"ask": 4.4,
							"last": 3.7
						},
						"contract size": 100,
						"open interest": 14
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 3.45,
							"ask": 5.8,
							"last": 3.26
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.63,
							"ask": 5.3,
							"last": 2.8
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 2.18,
							"ask": 4.8,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 1.62,
							"ask": 4.3,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 1.14,
							"ask": 3.85,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.91,
							"ask": 2.12,
							"last": 1.1
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.76,
							"ask": 1.09,
							"last": 0.61
						},
						"contract size": 100,
						"open interest": 189
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.59,
							"ask": 2.38,
							"last": 0.77
						},
						"contract size": 100,
						"open interest": 38
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.46,
							"ask": 0.8,
							"last": 0.55
						},
						"contract size": 100,
						"open interest": 83
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.34,
							"ask": 0.64,
							"last": 0.45
						},
						"contract size": 100,
						"open interest": 208
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.22,
							"ask": 0.39,
							"last": 0.36
						},
						"contract size": 100,
						"open interest": 338
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.25,
							"ask": 0.35,
							"last": 0.26
						},
						"contract size": 100,
						"open interest": 371
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.1,
							"ask": 0.28,
							"last": 0.11
						},
						"contract size": 100,
						"open interest": 111
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.06,
							"ask": 0.54,
							"last": 0.13
						},
						"contract size": 100,
						"open interest": 42
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.27,
							"last": 0.33
						},
						"contract size": 100,
						"open interest": 38
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.07,
							"ask": 0.18,
							"last": 0.11
						},
						"contract size": 100,
						"open interest": 190
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.21,
							"last": 0.23
						},
						"contract size": 100,
						"open interest": 17
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.42,
							"last": 0.1
						},
						"contract size": 100,
						"open interest": 22
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.21,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.3,
							"last": 0.28
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.19,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.11,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 15
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.93,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.13,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.13,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.09,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.02,
							"ask": 0.15,
							"last": 0.11
						},
						"contract size": 100,
						"open interest": 578
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.28,
							"last": 0.19
						},
						"contract size": 100,
						"open interest": 662
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.2,
							"ask": 0.44,
							"last": 0.35
						},
						"contract size": 100,
						"open interest": 171
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.43,
							"ask": 0.68,
							"last": 0.61
						},
						"contract size": 100,
						"open interest": 363
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.66,
							"ask": 0.95,
							"last": 0.82
						},
						"contract size": 100,
						"open interest": 308
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.99,
							"ask": 1.22,
							"last": 1.32
						},
						"contract size": 100,
						"open interest": 571
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 1.25,
							"ask": 1.75,
							"last": 1.75
						},
						"contract size": 100,
						"open interest": 81
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 1.73,
							"ask": 2.32,
							"last": 1.98
						},
						"contract size": 100,
						"open interest": 24
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 2.14,
							"ask": 2.74,
							"last": 2.42
						},
						"contract size": 100,
						"open interest": 12
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 2.41,
							"ask": 3.6,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 3.05,
							"ask": 3.8,
							"last": 3.1
						},
						"contract size": 100,
						"open interest": 27
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 3.35,
							"ask": 4.55,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 3.4,
							"ask": 4.45,
							"last": 4.5
						},
						"contract size": 100,
						"open interest": 43
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 4.45,
							"ask": 5.1,
							"last": 4.55
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 4.85,
							"ask": 5.85,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 5.3,
							"ask": 5.9,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 5.7,
							"ask": 7.75,
							"last": 6.35
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 6.25,
							"ask": 7.5,
							"last": 7.35
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 6.75,
							"ask": 7.8,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			}
		},
		{
			"expiration": "2024-08-16",
			"calls": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.95,
							"ask": 4.4,
							"last": 4.75
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 3.45,
							"ask": 3.85,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.59,
							"ask": 4.05,
							"last": 4.5
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 2.43,
							"ask": 2.82,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 1.94,
							"ask": 2.33,
							"last": 3.65
						},
						"contract size": 100,
						"open interest": 5
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.91,
							"ask": 1.72,
							"last": 1.38
						},
						"contract size": 100,
						"open interest": 100
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.25,
							"ask": 1.32,
							"last": 1.16
						},
						"contract size": 100,
						"open interest": 1198
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.49,
							"ask": 1.13,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.73,
							"ask": 0.84,
							"last": 0.8
						},
						"contract size": 100,
						"open interest": 941
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.46,
							"ask": 1.15,
							"last": 0.64
						},
						"contract size": 100,
						"open interest": 191
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.44,
							"ask": 0.52,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 6572
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.39,
							"ask": 0.45,
							"last": 0.44
						},
						"contract size": 100,
						"open interest": 80
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.31,
							"ask": 0.37,
							"last": 0.34
						},
						"contract size": 100,
						"open interest": 5382
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.31,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.21,
							"ask": 0.26,
							"last": 0.23
						},
						"contract size": 100,
						"open interest": 3004
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.22,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.16,
							"ask": 0.18,
							"last": 0.18
						},
						"contract size": 100,
						"open interest": 15504
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.18,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.11,
							"ask": 0.15,
							"last": 0.15
						},
						"contract size": 100,
						"open interest": 637
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.15,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.09,
							"ask": 0.11,
							"last": 0.09
						},
						"contract size": 100,
						"open interest": 2899
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.16,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.07,
							"ask": 0.15,
							"last": 0.07
						},
						"contract size": 100,
						"open interest": 184
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.09,
							"last": 0.06
						},
						"contract size": 100,
						"open interest": 1161
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 0.03,
							"ask": 0.04,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 3956
					},
					{
						"strike": 16.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.04,
							"last": 0.06
						},
						"contract size": 100,
						"open interest": 319
					},
					{
						"strike": 17.0,
						"prices": {
							"bid": 0.02,
							"ask": 0.04,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 54
					},
					{
						"strike": 18.0,
						"prices": {
							"bid": 0.03,
							"ask": 0.04,
							"last": 0.08
						},
						"contract size": 100,
						"open interest": 163
					},
					{
						"strike": 19.0,
						"prices": {
							"bid": 0.01,
							"ask": 0.05,
							"last": 0.03
						},
						"contract size": 100,
						"open interest": 131
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.05,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 1011
					},
					{
						"strike": 21.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.02
						},
						"contract size": 100,
						"open interest": 2050
					},
					{
						"strike": 22.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.32,
							"last": 0.19
						},
						"contract size": 100,
						"open interest": 11
					},
					{
						"strike": 23.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.35,
							"last": 0.09
						},
						"contract size": 100,
						"open interest": 12
					},
					{
						"strike": 24.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.05,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 154
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.1,
							"last": 0.03
						},
						"contract size": 100,
						"open interest": 683
					},
					{
						"strike": 26.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.02,
							"last": 0.05
						},
						"contract size": 100,
						"open interest": 40
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.03,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 259
					},
					{
						"strike": 28.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.01,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 404
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.03,
							"last": 0.03
						},
						"contract size": 100,
						"open interest": 505
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.15,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.03,
							"ask": 0.04,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 478
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.11,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.12,
							"ask": 0.15,
							"last": 0.14
						},
						"contract size": 100,
						"open interest": 3434
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.19,
							"ask": 0.27,
							"last": 0.28
						},
						"contract size": 100,
						"open interest": 50
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.41,
							"ask": 0.44,
							"last": 0.44
						},
						"contract size": 100,
						"open interest": 10775
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.51,
							"ask": 0.71,
							"last": 0.78
						},
						"contract size": 100,
						"open interest": 18
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.93,
							"ask": 0.99,
							"last": 0.96
						},
						"contract size": 100,
						"open interest": 15624
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 1.27,
							"ask": 1.45,
							"last": 1.52
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 1.61,
							"ask": 1.85,
							"last": 1.7
						},
						"contract size": 100,
						"open interest": 10860
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 2.0,
							"ask": 2.28,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 2.4,
							"ask": 2.91,
							"last": 2.55
						},
						"contract size": 100,
						"open interest": 1660
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 2.83,
							"ask": 3.2,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 3.35,
							"ask": 3.55,
							"last": 3.8
						},
						"contract size": 100,
						"open interest": 538
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 3.65,
							"ask": 4.05,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 3.5,
							"ask": 4.5,
							"last": 4.63
						},
						"contract size": 100,
						"open interest": 11518
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 4.65,
							"ask": 5.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 5.2,
							"ask": 5.4,
							"last": 5.67
						},
						"contract size": 100,
						"open interest": 132
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 5.55,
							"ask": 6.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 6.15,
							"ask": 6.4,
							"last": 6.16
						},
						"contract size": 100,
						"open interest": 86
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 6.6,
							"ask": 7.05,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 7.15,
							"ask": 7.4,
							"last": 7.28
						},
						"contract size": 100,
						"open interest": 30
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 8.15,
							"ask": 8.35,
							"last": 7.6
						},
						"contract size": 100,
						"open interest": 196
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 9.1,
							"ask": 9.4,
							"last": 8.93
						},
						"contract size": 100,
						"open interest": 227
					},
					{
						"strike": 16.0,
						"prices": {
							"bid": 10.15,
							"ask": 10.35,
							"last": 10.35
						},
						"contract size": 100,
						"open interest": 36
					},
					{
						"strike": 17.0,
						"prices": {
							"bid": 11.1,
							"ask": 11.35,
							"last": 10.58
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 18.0,
						"prices": {
							"bid": 12.0,
							"ask": 12.35,
							"last": 11.07
						},
						"contract size": 100,
						"open interest": 39
					},
					{
						"strike": 19.0,
						"prices": {
							"bid": 12.75,
							"ask": 13.8,
							"last": 12.46
						},
						"contract size": 100,
						"open interest": 13
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 13.95,
							"ask": 14.35,
							"last": 14.05
						},
						"contract size": 100,
						"open interest": 17
					},
					{
						"strike": 21.0,
						"prices": {
							"bid": 15.05,
							"ask": 15.4,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 22.0,
						"prices": {
							"bid": 15.05,
							"ask": 16.35,
							"last": 15.05
						},
						"contract size": 100,
						"open interest": 14
					},
					{
						"strike": 23.0,
						"prices": {
							"bid": 16.75,
							"ask": 18.1,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 24.0,
						"prices": {
							"bid": 18.0,
							"ask": 18.5,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 17.75,
							"ask": 19.3,
							"last": 18.95
						},
						"contract size": 100,
						"open interest": 77
					},
					{
						"strike": 26.0,
						"prices": {
							"bid": 19.4,
							"ask": 20.5,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 21.0,
							"ask": 21.55,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 28.0,
						"prices": {
							"bid": 21.85,
							"ask": 22.65,
							"last": 21.55
						},
						"contract size": 100,
						"open interest": 39
					}
				]
			}
		},
		{
			"expiration": "2024-08-23",
			"calls": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.3,
							"ask": 6.25,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 3.4,
							"ask": 5.75,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.62,
							"ask": 5.05,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 2.41,
							"ask": 4.75,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.2,
							"ask": 4.25,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.0,
							"ask": 3.8,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.11,
							"ask": 3.45,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.36,
							"ask": 3.15,
							"last": 1.28
						},
						"contract size": 100,
						"open interest": 20
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.51,
							"ask": 2.94,
							"last": 0.64
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.46,
							"ask": 0.72,
							"last": 0.7
						},
						"contract size": 100,
						"open interest": 40
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.47,
							"last": 0.43
						},
						"contract size": 100,
						"open interest": 142
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.12,
							"ask": 0.69,
							"last": 0.49
						},
						"contract size": 100,
						"open interest": 47
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.7,
							"last": 0.67
						},
						"contract size": 100,
						"open interest": 42
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.46,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.78,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.39,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.3,
							"last": 0.25
						},
						"contract size": 100,
						"open interest": 27
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.32,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.31,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.3,
							"last": 0.35
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.28,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.27,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.19,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.2,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.7,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.27,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.17,
							"ask": 0.25,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 10
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.44,
							"last": 0.4
						},
						"contract size": 100,
						"open interest": 20
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.26,
							"ask": 1.15,
							"last": 0.52
						},
						"contract size": 100,
						"open interest": 46
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.17,
							"ask": 1.05,
							"last": 0.77
						},
						"contract size": 100,
						"open interest": 110
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.41,
							"ask": 1.49,
							"last": 1.05
						},
						"contract size": 100,
						"open interest": 219
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.61,
							"ask": 3.5,
							"last": 1.3
						},
						"contract size": 100,
						"open interest": 250
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.01,
							"ask": 3.75,
							"last": 1.71
						},
						"contract size": 100,
						"open interest": 79
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.23,
							"ask": 4.3,
							"last": 2.05
						},
						"contract size": 100,
						"open interest": 78
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.5,
							"ask": 4.7,
							"last": 2.4
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 1.09,
							"ask": 5.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 1.4,
							"ask": 5.65,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 2.07,
							"ask": 6.1,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 2.34,
							"ask": 6.4,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 2.82,
							"ask": 6.9,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 3.25,
							"ask": 7.4,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 3.75,
							"ask": 8.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 4.25,
							"ask": 8.5,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 4.75,
							"ask": 9.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 5.25,
							"ask": 9.5,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			}
		},
		{
			"expiration": "2024-08-30",
			"calls": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.9,
							"ask": 5.95,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 3.45,
							"ask": 5.45,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.78,
							"ask": 5.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 2.15,
							"ask": 4.45,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.07,
							"ask": 4.15,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.24,
							"ask": 2.8,
							"last": 2.7
						},
						"contract size": 100,
						"open interest": 200
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.13,
							"ask": 2.83,
							"last": 1.3
						},
						"contract size": 100,
						"open interest": 12
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.12,
							"ask": 3.15,
							"last": 1.3
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.41,
							"ask": 1.54,
							"last": 0.85
						},
						"contract size": 100,
						"open interest": 23
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.42,
							"ask": 0.9,
							"last": 0.75
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.36,
							"ask": 2.64,
							"last": 0.4
						},
						"contract size": 100,
						"open interest": 54
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.0,
							"ask": 2.52,
							"last": 0.35
						},
						"contract size": 100,
						"open interest": 22
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.66,
							"last": 0.31
						},
						"contract size": 100,
						"open interest": 23
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 0.25,
							"ask": 0.75,
							"last": 0.35
						},
						"contract size": 100,
						"open interest": 9
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.8,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 22
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 0.0,
							"ask": 1.69,
							"last": 0.59
						},
						"contract size": 100,
						"open interest": 5
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.67,
							"last": 0.59
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 0.0,
							"ask": 1.29,
							"last": 0.56
						},
						"contract size": 100,
						"open interest": 16
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.09,
							"last": 0.42
						},
						"contract size": 100,
						"open interest": 28
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 0.0,
							"ask": 1.63,
							"last": 0.09
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.94,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.13,
							"ask": 0.43,
							"last": 0.13
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.49,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.98,
							"last": 0.42
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.3,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.3,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.5,
						"prices": {
							"bid": 0.0,
							"ask": 1.51,
							"last": 0.17
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.11,
							"ask": 0.99,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 42
					},
					{
						"strike": 4.5,
						"prices": {
							"bid": 0.25,
							"ask": 0.45,
							"last": 0.45
						},
						"contract size": 100,
						"open interest": 43
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.46,
							"ask": 0.73,
							"last": 0.8
						},
						"contract size": 100,
						"open interest": 69
					},
					{
						"strike": 5.5,
						"prices": {
							"bid": 0.7,
							"ask": 1.05,
							"last": 0.97
						},
						"contract size": 100,
						"open interest": 279
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.63,
							"ask": 1.88,
							"last": 1.4
						},
						"contract size": 100,
						"open interest": 90
					},
					{
						"strike": 6.5,
						"prices": {
							"bid": 0.48,
							"ask": 1.99,
							"last": 1.78
						},
						"contract size": 100,
						"open interest": 23
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.32,
							"ask": 2.51,
							"last": 2.13
						},
						"contract size": 100,
						"open interest": 45
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 1.07,
							"ask": 3.15,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.98,
							"ask": 3.25,
							"last": 2.65
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 8.5,
						"prices": {
							"bid": 1.66,
							"ask": 3.65,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 1.91,
							"ask": 4.6,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 9.5,
						"prices": {
							"bid": 2.5,
							"ask": 4.9,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 4.2,
							"ask": 5.95,
							"last": 4.42
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 10.5,
						"prices": {
							"bid": 3.15,
							"ask": 7.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 3.65,
							"ask": 7.8,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 11.5,
						"prices": {
							"bid": 4.6,
							"ask": 6.75,
							"last": 6.05
						},
						"contract size": 100,
						"open interest": 5
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 6.0,
							"ask": 7.35,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 6.25,
							"ask": 8.35,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 5.9,
							"ask": 9.9,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			}
		},
		{
			"expiration": "2024-09-20",
			"calls": {
				"strikes": [
					{
						"strike": 1.0,
						"prices": {
							"bid": 3.35,
							"ask": 6.5,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 2.0,
						"prices": {
							"bid": 2.89,
							"ask": 5.05,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 1.89,
							"ask": 3.5,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 1.11,
							"ask": 2.54,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.91,
							"ask": 1.55,
							"last": 1.4
						},
						"contract size": 100,
						"open interest": 6
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 0.91,
							"ask": 1.33,
							"last": 0.92
						},
						"contract size": 100,
						"open interest": 13
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.65,
							"ask": 0.74,
							"last": 0.73
						},
						"contract size": 100,
						"open interest": 168
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.35,
							"ask": 0.59,
							"last": 0.53
						},
						"contract size": 100,
						"open interest": 11
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.33,
							"ask": 0.53,
							"last": 0.37
						},
						"contract size": 100,
						"open interest": 39
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.27,
							"ask": 0.43,
							"last": 0.28
						},
						"contract size": 100,
						"open interest": 149
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.11,
							"ask": 0.25,
							"last": 0.25
						},
						"contract size": 100,
						"open interest": 52
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.31,
							"last": 0.18
						},
						"contract size": 100,
						"open interest": 6
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.27,
							"last": 0.3
						},
						"contract size": 100,
						"open interest": 1
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 1.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.22,
							"last": 0.01
						},
						"contract size": 100,
						"open interest": 30
					},
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.95,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.01,
							"ask": 0.44,
							"last": 0.1
						},
						"contract size": 100,
						"open interest": 100
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.17,
							"ask": 0.39,
							"last": 0.35
						},
						"contract size": 100,
						"open interest": 122
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.71,
							"ask": 1.1,
							"last": 0.81
						},
						"contract size": 100,
						"open interest": 523
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 1.3,
							"ask": 1.51,
							"last": 1.46
						},
						"contract size": 100,
						"open interest": 13
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 2.02,
							"ask": 2.66,
							"last": 2.22
						},
						"contract size": 100,
						"open interest": 17
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 2.73,
							"ask": 3.4,
							"last": 3.04
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 3.1,
							"ask": 4.35,
							"last": 4.0
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 4.45,
							"ask": 5.05,
							"last": 5.01
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 3.8,
							"ask": 6.15,
							"last": 5.68
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 6.0,
							"ask": 8.65,
							"last": 6.85
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 6.1,
							"ask": 8.25,
							"last": 8.0
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			}
		},
		{
			"expiration": "2024-11-15",
			"calls": {
				"strikes": [
					{
						"strike": 1.0,
						"prices": {
							"bid": 4.9,
							"ask": 5.4,
							"last": 5.65
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.9,
							"ask": 4.4,
							"last": 4.65
						},
						"contract size": 100,
						"open interest": 10
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.63,
							"ask": 3.4,
							"last": 3.5
						},
						"contract size": 100,
						"open interest": 14
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 2.03,
							"ask": 2.6,
							"last": 2.15
						},
						"contract size": 100,
						"open interest": 22
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.4,
							"ask": 2.9,
							"last": 1.6
						},
						"contract size": 100,
						"open interest": 384
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 1.01,
							"ask": 2.5,
							"last": 1.06
						},
						"contract size": 100,
						"open interest": 273
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.78,
							"ask": 1.19,
							"last": 1.02
						},
						"contract size": 100,
						"open interest": 1363
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.61,
							"ask": 0.98,
							"last": 0.7
						},
						"contract size": 100,
						"open interest": 1218
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.51,
							"ask": 0.8,
							"last": 0.59
						},
						"contract size": 100,
						"open interest": 282
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.5,
							"ask": 0.54,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 694
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.29,
							"ask": 0.45,
							"last": 0.55
						},
						"contract size": 100,
						"open interest": 280
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.26,
							"ask": 0.53,
							"last": 0.49
						},
						"contract size": 100,
						"open interest": 240
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.47,
							"last": 0.34
						},
						"contract size": 100,
						"open interest": 86
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 0.17,
							"ask": 0.43,
							"last": 0.3
						},
						"contract size": 100,
						"open interest": 27
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 0.17,
							"ask": 0.38,
							"last": 0.31
						},
						"contract size": 100,
						"open interest": 105
					},
					{
						"strike": 16.0,
						"prices": {
							"bid": 0.11,
							"ask": 0.37,
							"last": 0.36
						},
						"contract size": 100,
						"open interest": 251
					},
					{
						"strike": 17.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.43,
							"last": 0.32
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 18.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.54,
							"last": 0.3
						},
						"contract size": 100,
						"open interest": 29
					},
					{
						"strike": 19.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.93,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 7
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 0.04,
							"ask": 0.25,
							"last": 0.16
						},
						"contract size": 100,
						"open interest": 34
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 1.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.3,
							"last": 0.04
						},
						"contract size": 100,
						"open interest": 11
					},
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.15,
							"ask": 0.21,
							"last": 0.16
						},
						"contract size": 100,
						"open interest": 358
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.35,
							"ask": 0.37,
							"last": 0.37
						},
						"contract size": 100,
						"open interest": 465
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.6,
							"ask": 1.06,
							"last": 0.8
						},
						"contract size": 100,
						"open interest": 6836
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.19,
							"ask": 1.46,
							"last": 1.45
						},
						"contract size": 100,
						"open interest": 4691
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 1.89,
							"ask": 2.77,
							"last": 2.3
						},
						"contract size": 100,
						"open interest": 999
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 2.68,
							"ask": 3.15,
							"last": 3.1
						},
						"contract size": 100,
						"open interest": 1160
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 3.5,
							"ask": 4.1,
							"last": 4.02
						},
						"contract size": 100,
						"open interest": 267
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 4.35,
							"ask": 5.0,
							"last": 4.86
						},
						"contract size": 100,
						"open interest": 267
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 5.25,
							"ask": 6.55,
							"last": 5.5
						},
						"contract size": 100,
						"open interest": 91
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 6.15,
							"ask": 6.75,
							"last": 6.43
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 6.25,
							"ask": 7.75,
							"last": 6.82
						},
						"contract size": 100,
						"open interest": 40
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 8.0,
							"ask": 8.7,
							"last": 8.13
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 9.0,
							"ask": 10.4,
							"last": 8.8
						},
						"contract size": 100,
						"open interest": 47
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 9.85,
							"ask": 10.6,
							"last": 10.55
						},
						"contract size": 100,
						"open interest": 84
					},
					{
						"strike": 16.0,
						"prices": {
							"bid": 10.8,
							"ask": 13.05,
							"last": 11.31
						},
						"contract size": 100,
						"open interest": 7
					},
					{
						"strike": 17.0,
						"prices": {
							"bid": 10.9,
							"ask": 14.0,
							"last": 11.48
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 18.0,
						"prices": {
							"bid": 12.45,
							"ask": 14.95,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 19.0,
						"prices": {
							"bid": 12.95,
							"ask": 16.0,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 13.9,
							"ask": 16.1,
							"last": 14.9
						},
						"contract size": 100,
						"open interest": 29
					}
				]
			}
		},
		{
			"expiration": "2025-01-17",
			"calls": {
				"strikes": [
					{
						"strike": 2.5,
						"prices": {
							"bid": 2.86,
							"ask": 3.9,
							"last": 3.4
						},
						"contract size": 100,
						"open interest": 71
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 2.09,
							"ask": 3.45,
							"last": 2.1
						},
						"contract size": 100,
						"open interest": 63
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.44,
							"ask": 1.79,
							"last": 1.52
						},
						"contract size": 100,
						"open interest": 1622
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 1.13,
							"ask": 1.77,
							"last": 1.2
						},
						"contract size": 100,
						"open interest": 1595
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 0.81,
							"ask": 1.27,
							"last": 0.91
						},
						"contract size": 100,
						"open interest": 5506
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.6,
							"ask": 0.91,
							"last": 0.75
						},
						"contract size": 100,
						"open interest": 2091
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.59,
							"ask": 0.76,
							"last": 0.62
						},
						"contract size": 100,
						"open interest": 14112
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.39,
							"ask": 0.72,
							"last": 0.67
						},
						"contract size": 100,
						"open interest": 121
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 0.48,
							"ask": 0.55,
							"last": 0.54
						},
						"contract size": 100,
						"open interest": 3683
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 0.24,
							"ask": 0.58,
							"last": 0.65
						},
						"contract size": 100,
						"open interest": 18
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 0.4,
							"ask": 0.49,
							"last": 0.36
						},
						"contract size": 100,
						"open interest": 3549
					},
					{
						"strike": 16.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.72,
							"last": 0.37
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 17.5,
						"prices": {
							"bid": 0.0,
							"ask": 0.52,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 4418
					},
					{
						"strike": 19.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.68,
							"last": 0.3
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 0.2,
							"ask": 0.35,
							"last": 0.25
						},
						"contract size": 100,
						"open interest": 7193
					},
					{
						"strike": 22.5,
						"prices": {
							"bid": 0.11,
							"ask": 0.51,
							"last": 0.32
						},
						"contract size": 100,
						"open interest": 241
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 0.15,
							"ask": 0.32,
							"last": 0.15
						},
						"contract size": 100,
						"open interest": 5868
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 0.12,
							"ask": 0.29,
							"last": 0.3
						},
						"contract size": 100,
						"open interest": 558
					},
					{
						"strike": 30.0,
						"prices": {
							"bid": 0.1,
							"ask": 0.18,
							"last": 0.18
						},
						"contract size": 100,
						"open interest": 1799
					},
					{
						"strike": 32.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.24,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 182
					},
					{
						"strike": 35.0,
						"prices": {
							"bid": 0.1,
							"ask": 0.22,
							"last": 0.1
						},
						"contract size": 100,
						"open interest": 274
					},
					{
						"strike": 37.0,
						"prices": {
							"bid": 0.02,
							"ask": 0.12,
							"last": 0.2
						},
						"contract size": 100,
						"open interest": 762
					},
					{
						"strike": 40.0,
						"prices": {
							"bid": 0.1,
							"ask": 0.11,
							"last": 0.1
						},
						"contract size": 100,
						"open interest": 5239
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 2.5,
						"prices": {
							"bid": 0.43,
							"ask": 0.48,
							"last": 0.48
						},
						"contract size": 100,
						"open interest": 8092
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.7,
							"ask": 1.41,
							"last": 1.27
						},
						"contract size": 100,
						"open interest": 5307
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.78,
							"ask": 2.06,
							"last": 2.0
						},
						"contract size": 100,
						"open interest": 45916
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 2.4,
							"ask": 4.1,
							"last": 2.83
						},
						"contract size": 100,
						"open interest": 5399
					},
					{
						"strike": 7.5,
						"prices": {
							"bid": 3.6,
							"ask": 4.15,
							"last": 3.82
						},
						"contract size": 100,
						"open interest": 32707
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 4.85,
							"ask": 6.15,
							"last": 4.9
						},
						"contract size": 100,
						"open interest": 47
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 6.15,
							"ask": 6.3,
							"last": 6.3
						},
						"contract size": 100,
						"open interest": 12195
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 6.65,
							"ask": 8.15,
							"last": 6.73
						},
						"contract size": 100,
						"open interest": 3
					},
					{
						"strike": 12.5,
						"prices": {
							"bid": 8.05,
							"ask": 9.65,
							"last": 8.55
						},
						"contract size": 100,
						"open interest": 2583
					},
					{
						"strike": 14.0,
						"prices": {
							"bid": 9.4,
							"ask": 10.2,
							"last": 9.75
						},
						"contract size": 100,
						"open interest": 6
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 10.35,
							"ask": 11.05,
							"last": 10.55
						},
						"contract size": 100,
						"open interest": 1213
					},
					{
						"strike": 16.0,
						"prices": {
							"bid": 11.25,
							"ask": 12.9,
							"last": 11.76
						},
						"contract size": 100,
						"open interest": 35
					},
					{
						"strike": 17.5,
						"prices": {
							"bid": 12.7,
							"ask": 15.2,
							"last": 13.64
						},
						"contract size": 100,
						"open interest": 2995
					},
					{
						"strike": 19.0,
						"prices": {
							"bid": 14.15,
							"ask": 15.65,
							"last": 14.15
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 15.15,
							"ask": 16.7,
							"last": 15.36
						},
						"contract size": 100,
						"open interest": 2688
					},
					{
						"strike": 22.5,
						"prices": {
							"bid": 17.55,
							"ask": 19.9,
							"last": 17.93
						},
						"contract size": 100,
						"open interest": 212
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 20.0,
							"ask": 21.5,
							"last": 20.25
						},
						"contract size": 100,
						"open interest": 1764
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 21.0,
							"ask": 23.8,
							"last": 22.61
						},
						"contract size": 100,
						"open interest": 238
					},
					{
						"strike": 30.0,
						"prices": {
							"bid": 24.5,
							"ask": 27.35,
							"last": 25.15
						},
						"contract size": 100,
						"open interest": 321
					},
					{
						"strike": 32.0,
						"prices": {
							"bid": 26.55,
							"ask": 27.4,
							"last": 26.85
						},
						"contract size": 100,
						"open interest": 314
					},
					{
						"strike": 35.0,
						"prices": {
							"bid": 29.65,
							"ask": 31.15,
							"last": 29.75
						},
						"contract size": 100,
						"open interest": 387
					},
					{
						"strike": 37.0,
						"prices": {
							"bid": 31.3,
							"ask": 33.2,
							"last": 32.6
						},
						"contract size": 100,
						"open interest": 68
					},
					{
						"strike": 40.0,
						"prices": {
							"bid": 34.5,
							"ask": 36.05,
							"last": 34.7
						},
						"contract size": 100,
						"open interest": 774
					}
				]
			}
		},
		{
			"expiration": "2025-02-21",
			"calls": {
				"strikes": [
					{
						"strike": 1.0,
						"prices": {
							"bid": 3.7,
							"ask": 6.65,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 2.0,
						"prices": {
							"bid": 3.3,
							"ask": 4.4,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.11,
							"ask": 3.45,
							"last": 3.9
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 1.47,
							"ask": 2.7,
							"last": 2.01
						},
						"contract size": 100,
						"open interest": 38
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 0.93,
							"ask": 2.36,
							"last": 1.62
						},
						"contract size": 100,
						"open interest": 1667
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 1.12,
							"ask": 2.31,
							"last": 1.42
						},
						"contract size": 100,
						"open interest": 63
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 0.18,
							"ask": 1.42,
							"last": 1.29
						},
						"contract size": 100,
						"open interest": 45
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 0.8,
							"ask": 1.48,
							"last": 0.8
						},
						"contract size": 100,
						"open interest": 26
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 0.42,
							"ask": 1.69,
							"last": 0.7
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.7,
							"ask": 0.91,
							"last": 0.64
						},
						"contract size": 100,
						"open interest": 18
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 0.1,
							"ask": 0.87,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.48,
							"last": 0.9
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.0,
							"ask": 1.39,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 1.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.29,
							"last": 0.16
						},
						"contract size": 100,
						"open interest": 23
					},
					{
						"strike": 2.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.29,
							"last": 0.34
						},
						"contract size": 100,
						"open interest": 7
					},
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.8,
							"last": 0.7
						},
						"contract size": 100,
						"open interest": 8
					},
					{
						"strike": 4.0,
						"prices": {
							"bid": 0.42,
							"ask": 3.45,
							"last": 1.2
						},
						"contract size": 100,
						"open interest": 12
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.81,
							"ask": 2.25,
							"last": 2.24
						},
						"contract size": 100,
						"open interest": 1671
					},
					{
						"strike": 6.0,
						"prices": {
							"bid": 2.51,
							"ask": 3.4,
							"last": 2.91
						},
						"contract size": 100,
						"open interest": 11
					},
					{
						"strike": 7.0,
						"prices": {
							"bid": 3.4,
							"ask": 3.9,
							"last": 3.5
						},
						"contract size": 100,
						"open interest": 11
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 4.2,
							"ask": 5.2,
							"last": 4.2
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 9.0,
						"prices": {
							"bid": 4.1,
							"ask": 6.15,
							"last": None
						},
						"contract size": 100,
						"open interest": 0
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 4.7,
							"ask": 6.55,
							"last": 6.11
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 11.0,
						"prices": {
							"bid": 6.45,
							"ask": 7.55,
							"last": 6.71
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 6.65,
							"ask": 8.45,
							"last": 7.64
						},
						"contract size": 100,
						"open interest": 1
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 7.4,
							"ask": 9.4,
							"last": 9.12
						},
						"contract size": 100,
						"open interest": 6
					}
				]
			}
		},
		{
			"expiration": "2025-12-19",
			"calls": {
				"strikes": [
					{
						"strike": 3.0,
						"prices": {
							"bid": 0.94,
							"ask": 4.9,
							"last": 3.0
						},
						"contract size": 100,
						"open interest": 58
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.5,
							"ask": 2.71,
							"last": 1.8
						},
						"contract size": 100,
						"open interest": 403
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 1.1,
							"ask": 2.28,
							"last": 1.21
						},
						"contract size": 100,
						"open interest": 2977
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.83,
							"ask": 1.27,
							"last": 1.78
						},
						"contract size": 100,
						"open interest": 1387
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 0.61,
							"ask": 1.3,
							"last": 0.85
						},
						"contract size": 100,
						"open interest": 165
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 0.49,
							"ask": 0.88,
							"last": 0.73
						},
						"contract size": 100,
						"open interest": 1755
					},
					{
						"strike": 18.0,
						"prices": {
							"bid": 0.31,
							"ask": 0.55,
							"last": 0.99
						},
						"contract size": 100,
						"open interest": 199
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 0.4,
							"ask": 0.55,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 602
					},
					{
						"strike": 22.0,
						"prices": {
							"bid": 0.33,
							"ask": 2.78,
							"last": 0.62
						},
						"contract size": 100,
						"open interest": 96
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 0.28,
							"ask": 2.71,
							"last": 0.4
						},
						"contract size": 100,
						"open interest": 390
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 0.28,
							"ask": 1.33,
							"last": 0.7
						},
						"contract size": 100,
						"open interest": 30
					},
					{
						"strike": 30.0,
						"prices": {
							"bid": 0.25,
							"ask": 1.05,
							"last": 0.39
						},
						"contract size": 100,
						"open interest": 355
					},
					{
						"strike": 32.0,
						"prices": {
							"bid": 0.13,
							"ask": 0.54,
							"last": 0.29
						},
						"contract size": 100,
						"open interest": 66
					},
					{
						"strike": 35.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.58,
							"last": 0.47
						},
						"contract size": 100,
						"open interest": 79
					},
					{
						"strike": 37.0,
						"prices": {
							"bid": 0.0,
							"ask": 2.55,
							"last": 0.31
						},
						"contract size": 100,
						"open interest": 130
					},
					{
						"strike": 40.0,
						"prices": {
							"bid": 0.25,
							"ask": 0.5,
							"last": 0.3
						},
						"contract size": 100,
						"open interest": 1914
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 3.0,
						"prices": {
							"bid": 1.02,
							"ask": 1.59,
							"last": 1.58
						},
						"contract size": 100,
						"open interest": 4304
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 3.0,
							"ask": 3.2,
							"last": 3.2
						},
						"contract size": 100,
						"open interest": 2171
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 5.5,
							"ask": 6.05,
							"last": 5.99
						},
						"contract size": 100,
						"open interest": 1667
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 7.25,
							"ask": 7.8,
							"last": 7.8
						},
						"contract size": 100,
						"open interest": 2007
					},
					{
						"strike": 13.0,
						"prices": {
							"bid": 10.0,
							"ask": 12.0,
							"last": 10.1
						},
						"contract size": 100,
						"open interest": 67
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 11.15,
							"ask": 12.45,
							"last": 12.08
						},
						"contract size": 100,
						"open interest": 354
					},
					{
						"strike": 18.0,
						"prices": {
							"bid": 12.85,
							"ask": 15.35,
							"last": 14.95
						},
						"contract size": 100,
						"open interest": 82
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 16.15,
							"ask": 17.15,
							"last": 16.85
						},
						"contract size": 100,
						"open interest": 292
					},
					{
						"strike": 22.0,
						"prices": {
							"bid": 18.15,
							"ask": 19.0,
							"last": 18.28
						},
						"contract size": 100,
						"open interest": 7
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 19.6,
							"ask": 22.1,
							"last": 21.51
						},
						"contract size": 100,
						"open interest": 226
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 22.8,
							"ask": 23.8,
							"last": 22.93
						},
						"contract size": 100,
						"open interest": 4
					},
					{
						"strike": 30.0,
						"prices": {
							"bid": 25.6,
							"ask": 26.65,
							"last": 26.05
						},
						"contract size": 100,
						"open interest": 159
					},
					{
						"strike": 32.0,
						"prices": {
							"bid": 26.25,
							"ask": 30.1,
							"last": 27.14
						},
						"contract size": 100,
						"open interest": 20
					},
					{
						"strike": 35.0,
						"prices": {
							"bid": 29.6,
							"ask": 31.45,
							"last": 30.4
						},
						"contract size": 100,
						"open interest": 37
					},
					{
						"strike": 37.0,
						"prices": {
							"bid": 31.55,
							"ask": 33.45,
							"last": 32.83
						},
						"contract size": 100,
						"open interest": 15
					},
					{
						"strike": 40.0,
						"prices": {
							"bid": 34.45,
							"ask": 36.35,
							"last": 36.06
						},
						"contract size": 100,
						"open interest": 1646
					}
				]
			}
		},
		{
			"expiration": "2026-01-16",
			"calls": {
				"strikes": [
					{
						"strike": 3.0,
						"prices": {
							"bid": 2.69,
							"ask": 3.9,
							"last": 2.78
						},
						"contract size": 100,
						"open interest": 177
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 1.7,
							"ask": 2.65,
							"last": 1.9
						},
						"contract size": 100,
						"open interest": 1874
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 1.06,
							"ask": 1.54,
							"last": 1.25
						},
						"contract size": 100,
						"open interest": 3149
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 0.91,
							"ask": 1.31,
							"last": 1.05
						},
						"contract size": 100,
						"open interest": 4124
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 0.75,
							"ask": 1.44,
							"last": 0.8
						},
						"contract size": 100,
						"open interest": 496
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 0.5,
							"ask": 0.95,
							"last": 0.8
						},
						"contract size": 100,
						"open interest": 1608
					},
					{
						"strike": 17.0,
						"prices": {
							"bid": 0.55,
							"ask": 0.9,
							"last": 0.91
						},
						"contract size": 100,
						"open interest": 74
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 0.42,
							"ask": 0.58,
							"last": 0.6
						},
						"contract size": 100,
						"open interest": 603
					},
					{
						"strike": 22.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.76,
							"last": 0.5
						},
						"contract size": 100,
						"open interest": 739
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 0.0,
							"ask": 0.77,
							"last": 0.62
						},
						"contract size": 100,
						"open interest": 14
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 0.4,
							"ask": 0.6,
							"last": 0.4
						},
						"contract size": 100,
						"open interest": 246
					}
				]
			},
			"puts": {
				"strikes": [
					{
						"strike": 3.0,
						"prices": {
							"bid": 1.68,
							"ask": 1.94,
							"last": 1.68
						},
						"contract size": 100,
						"open interest": 22669
					},
					{
						"strike": 5.0,
						"prices": {
							"bid": 3.0,
							"ask": 3.5,
							"last": 3.3
						},
						"contract size": 100,
						"open interest": 13500
					},
					{
						"strike": 8.0,
						"prices": {
							"bid": 5.55,
							"ask": 6.15,
							"last": 5.85
						},
						"contract size": 100,
						"open interest": 1046
					},
					{
						"strike": 10.0,
						"prices": {
							"bid": 7.25,
							"ask": 7.85,
							"last": 7.7
						},
						"contract size": 100,
						"open interest": 1168
					},
					{
						"strike": 12.0,
						"prices": {
							"bid": 9.05,
							"ask": 9.8,
							"last": 9.4
						},
						"contract size": 100,
						"open interest": 97
					},
					{
						"strike": 15.0,
						"prices": {
							"bid": 11.65,
							"ask": 12.5,
							"last": 12.13
						},
						"contract size": 100,
						"open interest": 53
					},
					{
						"strike": 17.0,
						"prices": {
							"bid": 12.15,
							"ask": 14.9,
							"last": 13.9
						},
						"contract size": 100,
						"open interest": 20
					},
					{
						"strike": 20.0,
						"prices": {
							"bid": 16.4,
							"ask": 17.7,
							"last": 16.92
						},
						"contract size": 100,
						"open interest": 58
					},
					{
						"strike": 22.0,
						"prices": {
							"bid": 18.2,
							"ask": 19.75,
							"last": 18.15
						},
						"contract size": 100,
						"open interest": 64
					},
					{
						"strike": 25.0,
						"prices": {
							"bid": 20.95,
							"ask": 22.8,
							"last": 21.33
						},
						"contract size": 100,
						"open interest": 2
					},
					{
						"strike": 27.0,
						"prices": {
							"bid": 21.7,
							"ask": 24.6,
							"last": 23.8
						},
						"contract size": 100,
						"open interest": 43
					}
				]
			}
		}
	]