
''''
from arcaic_trading.procedures.spacedate.days_between import calc_days_between
calc_days_between ('2024-07-23', '2026-07-23')
"'''

from datetime import datetime


def calc_days_between (date_1_string, date_2_string):
	date_1 = datetime.strptime (date_1_string, '%Y-%m-%d')
	date_2 = datetime.strptime (date_2_string, '%Y-%m-%d')
	difference = date_2 - date_1
	days_between = difference.days
	return days_between;