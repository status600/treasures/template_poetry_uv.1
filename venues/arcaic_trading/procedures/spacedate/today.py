


from datetime import datetime

def calc_today ():
	today_date = datetime.today().date()
	formatted_date = today_date.strftime('%Y-%m-%d')
	return formatted_date