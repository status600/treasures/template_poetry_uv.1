




'''
	from arcaic_trading.adventures.monetary.DB.arcaic_trading_inventory.foods.document.find import find_food
	find_food ({
		"filter": {
			"nature.identity.FDC ID": ""
		}
	})
'''



from arcaic_trading._essence import retrieve_essence
from arcaic_trading.adventures.monetary.DB.arcaic_trading_inventory.connect import connect_to_arcaic_trading_inventory
from arcaic_trading.besties.food_USDA.nature_v2._ops.retrieve import retrieve_parsed_USDA_food
	
import ships.modules.exceptions.parse as parse_exception



	
'''
	FDC_ID = "",
	affiliates = [],
	goodness_certifications = []
'''
def find_food (packet):
	filter = packet ["filter"]

	food = None

	try:
		[ driver, arcaic_trading_inventory_DB ] = connect_to_arcaic_trading_inventory ()
		food_collection = arcaic_trading_inventory_DB ["food"]
	except Exception as E:
		print ("food collection connect:", E)
		raise Exception (E)
	
	try:	
		essence = retrieve_essence ()
		food = food_collection.find_one (filter, {"_id": 0});
		
	except Exception as E:
		print (parse_exception.now (E))
		raise Exception (E)
		
	try:
		driver.close ()
	except Exception as E:
		print (parse_exception.now (E))
		print ("food collection disconnect exception:", E)	
		
	return food;








