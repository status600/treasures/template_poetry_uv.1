


'''	
	from arcaic_trading.adventures.monetary.DB.arcaic_trading_inventory.connect import connect_to_arcaic_trading_inventory
	[ driver, arcaic_trading_inventory_DB ] = connect_to_arcaic_trading_inventory ()
	driver.close ()
'''

'''
	from arcaic_trading.adventures.monetary.DB.arcaic_trading_inventory.connect import connect_to_arcaic_trading_inventory
	[ driver, arcaic_trading_inventory_DB ] = connect_to_arcaic_trading_inventory ()
	foods_collection = arcaic_trading_inventory_DB ["foods"]	
	foods_collection.close ()
'''




from arcaic_trading.adventures.monetary.moves.URL.retrieve import retreive_monetary_URL
from arcaic_trading._essence import retrieve_essence
	
import pymongo

def connect_to_arcaic_trading_inventory ():
	essence = retrieve_essence ()
	
	ingredients_DB_name = essence ["monetary"] ["databases"] ["template"] ["alias"]
	monetary_URL = retreive_monetary_URL ()

	driver = pymongo.MongoClient (monetary_URL)

	return [
		driver,
		driver [ ingredients_DB_name ]
	]